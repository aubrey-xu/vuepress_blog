// 导航栏的配置
module.exports = [
  {
    text: '我的其他站点',
    items: [
      { text: '仿掘金', link: 'http://juejin.xumaobin.xyz' },
      { text: '仿网易云音乐', link: 'https://xumaobin.xyz' },
      { text: '后台管理系统', link: 'http://manage.xumaobin.xyz' },
      { text: '免费图床', link: 'https://image.xumaobin.xyz' }
    ]
  },
  {
    text: 'Gitee',
    link: 'https://gitee.com/aubrey-xu/vuepress_blog/tree/gh-pages/'
  }
]
