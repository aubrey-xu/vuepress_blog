// 侧边栏的配置
module.exports = [
  {
    title: '学习记录', // 必要的
    // path: '/学习记录/', // 可选的, 标题的跳转链接，应为绝对路径且必须存在
    children: [
      {
        title: 'vue',
        children: [
          // 在此新建 vue 文章,记住用绝对路径
          '/学习记录/vue/组件通信',
          '/学习记录/vue/响应式原理',
          '/学习记录/vue/diff算法'
        ]
      },
      {
        title: 'ES6',
        children: [
          // 在此新建 ES6 文章,记住用绝对路径
          '/学习记录/ES6/js基础',
          '/学习记录/ES6/import标准'
        ]
      },
      {
        title: 'CSS',
        children: [
          // 在此新建 CSS 文章,记住用绝对路径
          '/学习记录/CSS/flex',
          '/学习记录/CSS/文本溢出'
        ]
      }
      // 这个那天有时间学了再来
      // {
      //   title: '小程序',
      //   children: [
      //     // 在此新建 小程序 文章,记住用绝对路径
      //     '/学习记录/小程序/分包与预下载'
      //   ]
      // }
    ]
  },
  {
    title: '项目要点',
    path: '/项目要点/',
    children: [
      {
        title: '仿掘金',
        children: [
          // 仿掘金
          '/项目要点/仿掘金/仿掘金'
        ]
      },
      {
        title: '旅猫音乐',
        children: [
          // 在此新建 旅猫音乐 文章,记住用绝对路径
          '/项目要点/旅猫音乐/旅猫音乐'
        ]
      },
      {
        title: '旅猫购物',
        children: [
          // 在此新建 旅猫购物 文章,记住用绝对路径
          '/项目要点/旅猫购物/旅猫购物'
        ]
      },
      {
        title: '后台管理系统',
        children: [
          // 在此新建 后台管理系统 文章,记住用绝对路径
          '/项目要点/后台管理系统/后台管理系统'
        ]
      }
    ]
  },
  {
    title: '工具使用', // 必要的
    children: [
      {
        title: 'markdown',
        children: [
          // 在此新建 markdown 文章,记住用绝对路径
          '/工具使用/markdown/markdown基本写法',
          '/工具使用/markdown/markdownExpand.md'
        ]
      },
      {
        title: 'Eslint&Prettier',
        children: [
          // 在此新建 eslint & prettier 文章,记住用绝对路径
          '/工具使用/Eslint&Prettier/Eslint&Prettier'
        ]
      }
    ]
  },
  {
    title: '面试面试',
    children: [
      // 在此新建 面试题 文章,记住用绝对路径
      '/面试面试/p1',
      '/面试面试/p2',
      '/面试面试/p3'
    ]
  },
  {
    title: 'leetCode',
    children: [
      {
        title: '简单题',
        children: [
          // leetCode 简单题
          '/leetCode/简单题/数组-两数之和',
          '/leetCode/简单题/回文数'
        ]
      },
      {
        title: '中等题',
        children: [
          // leetcode 中等题
          '/leetCode/中等题/链表-两数之和',
          '/leetCode/中等题/字符串-最长子串',
          '/leetCode/中等题/最长回文子串',
          '/leetCode/中等题/z字形变换',
          '/leetCode/中等题/整数反转',
          '/leetCode/中等题/字符串转整数-atoi',
          '/leetCode/中等题/盛最多水的容器',
          '/leetCode/中等题/整数转罗马数字'
        ]
      },
      {
        title: '困难题',
        children: [
          // leetcode 困难题
          '/leetCode/困难题/两个正序数组的中位数',
          '/leetCode/困难题/正则表达式匹配'
        ]
      }
    ]
  },
  {
    title: '服务端踩坑记',
    children: [
      // 在此新建 服务端踩坑记 文章,记住用绝对路径
      '/服务端踩坑记/git云端仓库的初始化',
      '/服务端踩坑记/7z压缩上传乱码问题',
      '/服务端踩坑记/git更改远程仓库指向',
      '/服务端踩坑记/git将打包文件部署到新分支'
    ]
  }
]
