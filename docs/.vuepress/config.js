// 主配置文件
module.exports = {
  // base: '/vuepress_blog/',
  // title: '旅猫',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { name: 'author', content: '旅猫' }],
    ['meta', { name: 'keywords', content: '博客,前端,旅猫,vue' }],
  ],
  themeConfig: {
    logo: '/img/logo.png',
    nav: require('./nav'),
    sidebar: require('./sidebar'),
    sidebarDepth: 2,
    lastUpdated: '上次编辑', // string | boolean
  },
  markdown: {
    lineNumbers: true,
  },
  plugins: [
    [
      // 看板娘插件
      '@vuepress-reco/vuepress-plugin-kan-ban-niang',
      {
        theme: ['koharu'],
        clean: false,
        messages: {
          welcome: '这里是旅猫的主页',
          message: '这里是旅猫的主页',
          home: '回到家辣',
          close: 'Oyasumi!',
        },
        modelStyle: {
          right: '10px',
          bottom: '-20px',
          opacity: '0.9',
        },
        btnStyle: {
          right: '10px',
          bottom: '20px',
        },
      },
    ],
    // 容器插件配置
    [
      'vuepress-plugin-container',
      {
        type: 'tip',
        defaultTitle: {
          '/': '',
        },
      },
    ],
    // 回到顶部
    // ['@vuepress/back-to-top']
  ],
}
