---
# home: true 
# heroImage: /hero.png
# heroText: Hero 标题
# tagline: Hero 副标题
# actionText:  →
# actionLink: /zh/guide/
# footer: MIT Licensed | Copyright © 2018-present Evan You
# title: git 云端仓库的初始化
---

::: tip 自动化部署

#### 每周一的5：16，服务器会自动拉取[云端仓库](https://gitee.com/aubrey-xu/vuepress-blog)的新文章 :arrow_heading_up:

##### ~~要是当初放到 GitHub Pages 也不用这么麻烦啊~~

::: 

