---
title: git 云端仓库的初始化
---
​	gitee 和 github 操作一样

1. 在项目目录初始化 git 与 npm 

   ```
   git init
   
   npm init -y
   ```

2. 在根目录下配置  .gitignore

   ```
   # 忽略 /dist node_modules/ ，其他可自己添加
   /dist
   node_modules
     
   # git 只会匹配同级目录，有时要在忽略目录新建 .gitignore
   ```

3. gitee 中新建仓库,地址选择 ssl 格式

   项目上传:

   ```
   // 添加本地仓库指向,地址视仓库而定
   git remote add origin git@gitee.com:aubrey-xu/uniapp_store.git
   
   git push -u origin "master"
   ```