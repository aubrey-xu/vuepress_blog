---
title: git打包上传新分支+自动化部署
---

## 新分支部署

 ::: tip

 原本我的打包文件要放到服务器时,需要自己压缩上传

 在看博客部署时,vuepress 有一个不错的方法

 :::

 **在根目录中的 deploy.sh 中:**

```sh
# deploy.sh
# 确保脚本抛出遇到的错误
set -e

# 生成静态文件
npm run build

# 进入生成的文件夹
cd docs/.vuepress/dist

# 如果是发布到自定义域名
# echo 'blog.xumaobin.xyz' > CNAME

git init
git add -A
git commit -m 'deploy'

# 如果发布到 https://<USERNAME>.github.io
# git push -f git@gitee.com:aubrey-xu/aubrey-xu.gitee.io.git master

# 如果发布到 https://<USERNAME>.github.io/<REPO>
git push -f git@gitee.com:aubrey-xu/vuepress_blog.git master:gh-pages

cd -
```

**运行流程很简单**

1. **检测错误**

2. **打包出 dist 文件并切换到该目录**

3. **git 初始化,提交到暂存区,准备提交**

4. ```sh
   git push -f git@gitee.com:aubrey-xu/vuepress_blog.git master:gh-pages
   ```

**该段代码中**

::: tip

-f : '强制推送',该操作会覆盖远程仓库,只会显示一次提交

master:gh-pages : '将本地 master 分支推送到云端,命名为 gh-pages'

:::

那么,在本地推送后,服务器后台运行

```sh
cd '我的网站根目录'
git clone '我的 gitee 分支地址'
mv * ../
```

就不用自己打包上了:duck: ~~不是还要自己去后台运行命令吗?~~

---

## 服务端自动化脚本

#### 对了，在服务器再写一个脚本,这样就不用自己慢慢敲了:cat:

```sh
# deploy.sh

# 确保抛出错误
set -e

# 切换到指定文件夹
cd /www/wwwroot/blog.xumaobin.xyz

# 移除文件夹下所有文件
rm -rf *

# 直接 clone gh-pages 分支
# 第一次运行需要输入账号密码，之后便不用 
git clone -b gh-pages https://gitee.com/aubrey-xu/vuepress_blog

# cd 到 clone 的项目目录
cd /www/wwwroot/blog.xumaobin.xyz/vuepress_blog

# 将所有文件移动到网站根目录，即上一级目录
mv * ../
```

**再用 crontab 把 deploy.sh 设置为定时任务就完成了**:white_check_mark:

```sh {3}
# /var/spool/cron/root 编辑该文件
# 每周一的 5：16 开启脚本
16 5 * * 1 /root/deploy.sh
```

::: tip crontab运行格式说明

```
	*    *    *    *    *    文件路径
	-    -    -    -    -
	|    |    |    |    |    
	|    |    |    |    +----- 星期几 (0 - 7) (Sunday=0 or 7)
	|    |    |    +---------- 月份 (1 - 12)
	|    |    +--------------- 几号 (1 - 31)
	|    +-------------------- 小时 (0 - 23)
	+------------------------- 分钟 (0 - 59)
```

:::
