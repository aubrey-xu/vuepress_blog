---
title: git更改远程仓库指向
---

### 该博客更改了远程仓库的地址

记录一下操作方法

```sh {3}
git remote -v  # 查看远端地址
git remote # 查看远端仓库名
git remote set-url origin https://gitee.com/aubrey-xu/vuepress_blog # 新地址
```

