---
title: Eslint&Prettier
---
**prettier 用于格式化代码
eslint 用于规范检查**
```javascript
{
    // 本地文件优先级最高
    "prettier.configPath": "C:\\Users\\Omori\\.prettierrc",
    "eslint.alwaysShowStatus": true,
    "prettier.trailingComma": "none",
    "prettier.semi": false,
    // 每行文字个数超出此限制将会被迫换行
    "prettier.printWidth": 300,
    // 使用单引号替换双引号
    "prettier.singleQuote": true,
    "prettier.arrowParens": "avoid",
    // 设置 .vue 文件中，HTML代码的格式化插件
    "vetur.format.defaultFormatter.html": "js-beautify-html",
    "vetur.ignoreProjectWarning": true,
    "vetur.format.defaultFormatterOptions": {
        "js-beautify-html": {
            "wrap_attributes": false
        },
        "prettier": {
            "printWidth": 300,
            "trailingComma": "none",
            "semi": false,
            "singleQuote": true,
            "arrowParens": "avoid"
        }
    },
    //ESlint插件配置
  "editor.codeActionsOnSave": {
    "source.fixAll": true
},
"editor.tabSize": 2,
"[vue]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
},
"[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
},
}
```

**eslint去除对组件名的限制**
```javascript
//.eslintrc.js中
overrides: [
    {
      files: ['src/components/**/*.vue'],
      rules: {
        'vue/multi-word-component-names': 0
      }
    }
  ]

//vue.config.js中
lintOnSave: false
```