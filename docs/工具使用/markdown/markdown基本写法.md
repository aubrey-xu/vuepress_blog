---
title: markdown 基本写法
---

## 1. 标题多级

::: tip 

1. ctrl 1\~6 表示 h1\~h6标签

2. \'# ' 表示h1标签，以此类推

:::

## 2. 有序列表

::: tip 

1. \'1. \'类型实现
2. 一次空格加序号，两次空格消除序号，↓键直接换行不加序号
3. tab 键实现降级，shift + tab 键实现升级
4. ctrl + shift + [ 选中文本为有序列表

:::

1. 列表
   1. 子列表

## 3. 无序列表

::: tip 

1. '* ' 或 '- ' 实现
2. 一次空格加序号，两次空格消除序号，↓键直接换行不加序号
3. tab 键实现降级，shift + tab 键实现升级
4. ctrl + shift + ] 选中文本为无序列表

:::

- 列表
  - 子列表

## 4. 任务列表

::: tip 

1. '* [ ] ' 实现
2. ctrl + shift + x 实现

:::

- [x] 任务no
  - [ ] 任务yes

## 5. 插入表格

::: tip 

1. ' |t1|t2|' 实现
2. ctrl + shift + backspace 删除行，ctrl + enter 添加行
3. alt + 方向键 换行换列	

:::

| name   | test    |
| ------ | ------- |
| Aubrey | oyasumi |

## 6. 代码块

::: tip 

ctrl + shift + k 实现

```js
const str = 'hello world'
```

:::

## 7. 插入图片

::: tip 

1. ctrl + shift + i
2. ! [图片alt] (图片链接 "图片title") 

:::

## 8. 其他

1. 语句块: 

   ```
   > log here
   > over
   ```

   > hello
   >
   > markdown
   >

2. 添加链接: 

   ```
   [关键词](链接地址 "（可选）添加一个标题")
   ```

   例如: [回到我的博客主页](http://blog.xumaobin.xyz "描述")

3. emoji:

   ```
   :emojiName:
   ```

   例如: :duck:  :100: :cat:

4. 字体格式

   ```
   <font color="" size="" face="">文字</font>
   ```

   例如: <font color="red">红色</font> 

5. 删除线: 

   ```
   ~~我被删除了~~
   ```

   例如: ~~我被删除了~~

6. 分割线 ：---
