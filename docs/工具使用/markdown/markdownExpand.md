---
title: markdown 拓展
---
::: danger 注意

​	此语法并非原生 markdown 语法,目前以 vuepress 为基础支持框架

:::

## 1. 语句块

```
// 前缀支持有 
// tip: 绿色
// danger: 红色
// warning: 黄色
// details: 灰色	
::: tip title
tip 消息
:::
```

::: tip 标题

​	一个语句块

:::

## 2. 代码高亮 

````
// 这里对 1,2,5 行进行高亮显示
``` js {1-2,5}
var i
const j
let k
var m
const n
```
````

``` js {1,2,5}
var i
const j
let k
var m
const n
```