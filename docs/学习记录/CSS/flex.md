---
title: flex
---

## 一、常用 flex  排列方式

### 1. stretch

​		**效果：自动拉伸元素到撑满父元素**

::: tip 作用对象

在 flex-direction 方向上，设置为 auto 的元素

若元素设置 max-width 等属性，撑开大小不会超过该值

:::

### 2. space-evenly 与 space-around

::: tip 效果区别

space-evenly：<u>平均分配父元素剩下的位置</u>

space-around： <u>各相邻元素之间的距离</u> 为 <u>首、尾元素到父边界距离的两倍</u>

:::

### 3. space-between

​		**效果：首尾元素各占据首尾，相邻元素距离相同**

### 4. 效果：

```
//space-evenly
-----------------------------
-----0-----0-----0-----0-----
-----------------------------

// space-around
----------------------------
---0------0------0------0---
----------------------------

// space-between
---------------------------
0------------0------------0
---------------------------
```

## 二、flex 作用轴

### 1. align-items 与 align-content

​		**定义 flex 容器中元素在交叉轴的排列方式**

::: tip 区别

**align-items** 可以<font color="red">应用于所有的flex容器</font>，它的作用是设置flex子项在每个flex行的交叉轴上的默认对齐方式

**align-content** <font color="red">只适用多行的flex容器</font>（也就是flex容器中的子项不止一行时该属性才有效果），它的作用是当flex容器在交叉轴上有多余的空间时，<font color="red">将子项作为一个整体</font>（属性值为：flex-start、flex-end、center时）进行对齐

:::

### 2. align-self  与 justify-self

​		**设置自身在交叉轴上相对于父盒子的排列方式**

### 3. justify-content 与 justify-items 

::: tip 用途

**justify-content** 相当于主轴的 align-items，基本无脑在主轴用

**justify-items** 在宫格（grid）中可能用到，针对网格中容器的项，少用

:::

## 三，我的常用 flex

### 1.  小程序常用盒子

​		**子盒子相对父盒子居中且有一定边距**

```css {10,12}
// 父盒子
.father {
  width: 100%;
  background-color: #b3d4db;
  display: flex;
}

// 子盒子
.son {
  width: auto;
  height: 40rpx;
  padding: 20rpx;
  background-color: white;
}
```

### 2.  内容块

​		**第一行为标题占一行，剩下占一行或多行**

```css {7,12,18}
// 父盒子
.father {
	width: 100%;
	background-color: #b3d4db;
	display: flex;
	flex-direction: column;
  flex-wrap: wrap;
}

// 子盒子首行
.son-title {
	width: 100%;
	background-color: white;
}

// 子盒子内容
.son-content {
	width: 20%;
	background-color: white;;
}
```

