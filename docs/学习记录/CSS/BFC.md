### 1. 父级添加 overflow 属性，或者设置高度

### 2. 伪元素方式实现

```css
// 在css中添加:after伪元素
.clearfix:after{
    /* 设置添加子元素的内容是空 */
    content: '';
    /* 设置添加子元素为块级元素 */
    display: block;
    /* 设置添加的子元素的高度0 */
    height: 0;
    /* 设置添加子元素看不见 */
    visibility: hidden;
    /* 设置clear：both */
    clear: both;
}
```

