---
title: 导入导出的两种标准
---

::: tip 作用环境

1. **commonJS**
   - 作用于 node 环境
   - 分为 **require** 与  **module.exports**

2. **Es6modules**
   - 作用于 浏览器 环境,向下兼容
   - 分为 **import** 与 **export**

:::

## 一、require、mudule.exports 、exports

1. **rquire**

   - `require`是一个全局性方法，它的作用就是用于加载模块用的，使用该方法后会有个返回值，所以需要定义一个变量来接受，如下所示：

   ```js
   const { ipcRenderer } = require("electron");
   ```

2. **mudule.exports**

   - 在`commonJS`规范中每个文件就是一个模块，每个模块内部，有一个变量`module`代表当前模块。这个变量是一个对象，它的exports属性（即module.exports）是对外的接口。加载某个模块，其实是加载该模块的`module.exports`属性。

   ```js
   let a = 123;
   let b = [1,2,3];
   module.exports.a = a;
   module.exports.b = b;
   
   // 将a和b都挂载到了module.exports这个对象上，
   // 这两个变量将会随着module.exports这个对象一起导出
   ```

3. **exports**

   - 相当于下面这段代码 (鸡肋)

   ```js
   const exports = module.exports
   ```

## 二、import、export、export default

1. **import**
   - `import`也是一个命令，它和 'require '方法最大的区别就是：`import`命令在**编译时**就会将需要导入的文件或者变量函数等导进来，而`require`是在**运行时加载**的，有点类似于懒加载

2. **export、export default**
   - `export`和`export default`导出的值`import`的接收方式是不一样的


```js
// 导入 
import { a } from "./a.js";
import b from "./a.js"; //没有导出为 default 时，运行时会提示没有使用export default导出
import { c as fun } from "./a.js"; //导出时也可以使用as定义别名

// 导出
export { name1, name2};
export { a1 as name1, a2 as name2 };
export let name1, name2;
export let name1 = a1, name2 = a1;
 
export default expression;
export default function (a) { console.log(a) } 
export default function f1(a) { console.log(a) } 
export { name1 as default};
 
export * from name1;
export { name1, name2} from a1;
export { a1 as name1, a2 as name2 } from b;
```

