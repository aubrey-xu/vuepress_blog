---
title: js 基础方法总结
---

## 一、数组

### 1. push()  

::: tip

向数组的末尾添加一个或多个元素，并返回新的数组长度

原数组改变

:::

### 2. pop()

>删除并返回数组的最后一个元素，若该数组为空，则返回undefined
>
>原数组改变

### 3.    unshift()  

::: tip

向数组的开头添加一个或多个元素，并返回新的数组长度

原数组改变

:::

### 4. shift()

> 删除数组的第一项，并返回第一个元素的值。若该数组为空，则返回undefined
>
> 原数组改变

### 5. concat(arr1,arr2…)

> 合并两个或多个数组，生成一个新的数组
>
> 原数组不变

### 6.  join()

::: tip

将数组的每一项用指定字符连接形成一个字符串。默认连接字符为 “,” 逗号

:::

### 7. reverse()

> 将数组倒序
>
> 原数组改变

### 8.  sort()

::: tip 

对数组元素进行排序。按照字符串UniCode码排序

原数组改变

:::

::: warning  leetcode的问题

**sort()函数比较的是ASCII码的大小，而且而且而且：Array的sort()方法默认把所有元素先转换为String再排序，所以就有以下问题。**

```js
// baiDu排在了最后:
['Google', 'baiDu', 'Facebook'].sort(); // ['Facebook', 'Google", 'baiDu']
 
// 无法理解的结果:
[10, 20, 1, 2].sort(); // [1, 10, 2, 20]
```

**结果转换成字符串比较，'10'排在了'2'的前面，因为字符'1'比字符'2'的ASCII码小**

:::

::: warning 解决办法

```js
// 使用参数保证排序的稳定性
function compare(a,b){        
    return a-b
}

arr.sort(compare)
```

:::

### 9. map(function)

> 原数组的每一项执行函数后，返回一个新的数组
>
> 原数组不变。（注意该方法和forEach的区别）

### 10. slice() 

> 按照条件查找出其中的部分内容

### 11. splice( index , howmany , arr1 , arr2… )

::: tip

用于添加或删除数组中的元素。从index位置开始删除howmany个元素，并将arr1、arr2…数据从index位置依次插入。howmany为0时，则不删除元素

:::

### 12. filter()

::: tip

过滤数组中，符合条件的元素并返回一个新的数组

:::

### 13. forEach(function)

> 用于调用数组的每个元素，并将元素传递给回调函数
>
> 原数组不变（注意该方法和map的区别，若直接打印Array.forEach，结果为undefined）

### 14. every(function)

> 对数组中的每一项进行判断，若都符合则返回true，否则返回false

### 15. some(function)

> 对数组中的每一项进行判断，若都不符合则返回false，否则返回true

### 16. reduce(function)

> reduce() 方法接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值

### 17.indexOf()

> 检测当前值在数组中第一次出现的位置索引

### 18. includes()

> 判断一个数组是否包含一个指定的值

## 二、字符串

### 1. concat(str2,[str3…]) 

::: tip 

concat 方法将一个或多个字符串与原字符串连接合并，形成一个新的字符串并返回。

concat 方法并不 影响原字符串。

如果参数不是字符串类型，它们在连接之前将会被转换成字符串。

:::

### 2. indexOf() 

>str.indexOf(searchValue [, 可选fromIndex]) 第一个参数 **字符**，第二个参数 **开始位置**
>
>searchValue 要被查找的字符串值。**如果没有提供确切地提供字符串 ，会自动转为undefined**，然而在当前字符串查找这个值。‘undefined’.indexOf() 返回0 而’undefine’.indexOf返回-1
>
>检测字符是否存在 ‘blue red yellow’.indexOf(‘blue’)!==-1);
>
>indexOf区分大小写

### 3. chartAt() 

::: tip

返回指定位置的字符 根据下标获取字符

:::

### 4. lastIndexOf()

> 返回字符串字串出现的最后一处出现的位置索引 没有匹配的话返回-1

### 5. substr()

::: tip

str.substr(start[, length]) 起始位置 截取个数

返回起始位置startPos位置，长度为length的字符串

如果截取长度为0或者负值则返回一个空字符串

:::

### 6. substring()

>str.substring(indexStart[, indexEnd])
>
>返回字符串的一个字串，传入的是起始位置和结束位置

### 7. slice()

> str.slice(beginIndex[, endIndex])
>
> 提取字符的一部分，返回一个新字符 不会改变原字符串
>
> 参数代表起始位置和结束位置

### 8. match()

>检查一个字符串是否匹配一个正则表达式

### 9. test() 

>执行一个检索，用来查看正则表达式与指定的字符串是否匹配。返回 `true` 或 `false`。

### 10. replace() 

>用来查找匹配的正则表达式的字符串，然后使用新的字符串代替匹配的子字符串

### 11. search()

> 执行一个正则表达式匹配查找。如果查找成功，返回索引值 。否则返回-1

### 12. split() 

::: tip

str.split([separator[, limit]]) 

把字符串划分为字串，字串做成一个字符串数组

separator可以是一个字符也可以是正则表达式,默认以' , '为分割

limit一个整数 返回分割的数组长度 当在0-length之间会返回指定的数组长度，大于或小于都返回所有内容

:::

### 13. trim() 

::: tip

 删除字符串两端的空白符

:::

### 14. toLowerCase()

>将整个字符串转为小写

### 15. toUpperCase() 

>将整个字符串转为大写









