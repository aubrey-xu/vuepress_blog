### 项目地址

| 项目名称     | 开发框架 | 线上地址                                                 | gitee地址                                                    |
| ------------ | -------- | -------------------------------------------------------- | ------------------------------------------------------------ |
| 仿掘金       | vue      | [http://juejin.xumaobin.xyz](http://juejin.xumaobin.xyz) | [https://gitee.com/aubrey-xu/juejin](https://gitee.com/aubrey-xu/juejin) |
| 旅猫音乐     | vue      | [https://xumaobin.xyz](https://xumaobin.xyz)             | [https://gitee.com/aubrey-xu/vue_music163](https://gitee.com/aubrey-xu/vue_music163) |
| 旅猫购物     | uniapp   | 小程序                                                   | [https://gitee.com/aubrey-xu/uniapp_store](https://gitee.com/aubrey-xu/uniapp_store) |
| 后台管理系统 | vue      | [http://manage.xumaobin.xyz](http://manage.xumaobin.xyz) | [https://gitee.com/aubrey-xu/vue_backstage_manage](https://gitee.com/aubrey-xu/vue_backstage_manage) |

